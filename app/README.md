# Foolproof

> THe grilled prawn website build

## Getting Started

- Install dependencies: `npm install && bower install`
- Run `gulp serve` to run locally

## Frameworks/Technologies used
- Gulp
- Bower
- Sass
- Underscore.js
- IcoMoon
- Bourbon/Neat
- jQuery
- Velocity.js

