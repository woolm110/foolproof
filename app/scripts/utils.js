/*global $ */

var Foolproof = Foolproof || {};

Foolproof.utils = (function() {
  'use strict';

  return {

    /**
     * getData
     * get data from endpoint 
     * and fire callback
     * @param  {string}   url - endpoint
     * @param  {Function} cb - callback 
     */
    getData: function(url, cb) {
      $.getJSON(url).done(function(data) {
        if (typeof(cb) === 'function') {
          cb(data);
        }
      });
    },

    /**
     * createHtmlFromTemplate
     * populate script template with data
     * @param  {string} id - id of script template
     * @param  {obj} data - data to populate template
     * @return {Function}
     */
    createHtmlFromTemplate: function(id, data) {
      var template = _.template($(id).html());

      return template({ recipes: data });
    },

    /**
     * addHtmlToPage
     * add HTML to DOM element
     * @param {string} el - e.g. '.my-class'
     * @param {string} html - contetnt to add
     */
    addHtmlToPage: function(el, html) {
      $(el).html(html);
    },

    /**
     * scrollToPosition
     * scroll to elem in DOM
     */
    scrollToPosition: function() {
      var scrollToElem = $(this).data('scroll'),
        offset = $(scrollToElem).offset().top;

      $('body,html').velocity('scroll', {
        duration: 500,
        offset: offset
      });
    },
  };
}());
