'use strict';

window.addEventListener('DOMContentLoaded', function() {
  var Foolproof = this.Foolproof || {};

  for (var componentName in Foolproof) {
    var component = Foolproof[componentName];
    if (_.isObject(component) && component.init) {
      component.init();
    }
  }
});
