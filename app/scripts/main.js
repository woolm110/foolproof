/*global _, $ */

var Foolproof = Foolproof || {};

Foolproof.app = (function() {
  'use strict';

  var _private = {
    CONST: {},
    globals: {
      filters: []
    },

    /**
     * attachEventHandlers
     * Exactly like Ronseal
     */
    attachEventHandlers: function() {
      $('.content-filters [data-nationality]').on('click', _private.filterRecipes);
      $('[data-scroll]').on('click', Foolproof.utils.scrollToPosition);
      $('.icon-burger a').on('click', _private.toggleNavigation);
    },

    /**
     * filterRecipes
     * update HTML based on
     * select filters
     */
    filterRecipes: function() {
      var filters = _private.globals.filters,
        html = '',
        filteredObj = [];

      _private.updateFilterList(this.getAttribute('data-nationality'));

      // if we have filters to apply then return a subset of the data
      if (filters.length) {
        filteredObj = _.filter(_private.globals.data, function(recipe) {
          return filters.indexOf(recipe.nationality.toLowerCase()) !== -1;
        });

        html = Foolproof.utils.createHtmlFromTemplate('#recipe', filteredObj);

        if (!html.trim().length) {
          html = 'No recipes found.'
        }
      } else {
        html = Foolproof.utils.createHtmlFromTemplate('#recipe', _private.globals.data);
      }

      // update dom with new content
      Foolproof.utils.addHtmlToPage('.content-recipes .inner', html);
    },

    /**
     * updateFilterList
     * add/remove filters from
     * filters array
     * @param  {string} filter
     */
    updateFilterList: function(filter) {
      var filters = this.globals.filters,
        idx = filters.indexOf(filter);

      if (filter) {
        if (filters.indexOf(filter) === -1) {
          filters[filters.length] = filter;
        } else {
          filters.splice(idx, 1);
        }
      }
    },

    toggleNavigation: function() {
      var $navigation = $('.content-navigation ul');

      return $navigation.toggleClass('js-open');
    },

    /**
     * init
     * gets called on page load from init.js
     */
    init: function() {
      _private.attachEventHandlers();

      Foolproof.utils.getData('data/recipes.json', function(data) {
        var html;

        _private.globals.data = data; // persist data
        html = Foolproof.utils.createHtmlFromTemplate('#recipe', data);

        Foolproof.utils.addHtmlToPage('.content-recipes .inner', html);
      });
    }
  };

  return {
    init: _private.init
  };

}());
